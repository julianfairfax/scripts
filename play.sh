#!/bin/bash

url=$1
language=$2

if [[ $url == *playsuisse.ch* ]]; then
	if [[ -z $language ]]; then
		language=de
	fi

	yt-dlp -u $(cat ~/.playsuisse.txt | grep u: | sed 's/u: //') -p $(cat ~/.playsuisse.txt | grep p: | sed 's/p: //') -o - -f best/bestvideo+bestaudio[language=$language] $url | flatpak run com.github.rafostar.Clapper fd://0
else
	if [[ -z $language ]]; then
		language=en
	fi

	yt-dlp $url -o - | flatpak run com.github.rafostar.Clapper fd://0
fi
