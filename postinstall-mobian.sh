#!/bin/bash

# sudo apt purge --autoremove chatty epiphany-browser geary gnome-calculator gnome-calendar gnome-calls gnome-clocks gnome-contacts gnome-maps gnome-snapshot gnome-sound-recorder gnome-system-monitor gnome-text-editor gnome-usage gnome-weather evince loupe totem endeavour

sudo apt install nautilus

sudo flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo

sudo flatpak install --noninteractive net.nokyan.Resources org.gnome.baobab org.gnome.Calculator org.gnome.Calendar org.gnome.Characters org.gnome.clocks org.gnome.Connections org.gnome.Contacts org.gnome.Epiphany org.gnome.Evince org.gnome.font-viewer org.gnome.Logs org.gnome.Loupe org.gnome.Maps org.gnome.Music org.gnome.SimpleScan org.gnome.Snapshot org.gnome.TextEditor org.gnome.Weather org.gnome.World.PikaBackup
