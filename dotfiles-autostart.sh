#!/bin/bash

if [[ ! -d ~/.config/autostart ]]; then
	mkdir ~/.config/autostart
fi

if [[ -f /var/lib/flatpak/exports/share/applications/com.nextcloud.desktopclient.nextcloud.desktop && ! -f ~/.config/autostart/com.nextcloud.desktopclient.nextcloud.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=Nextcloud
Exec=flatpak run com.nextcloud.desktopclient.nextcloud --background
Icon=Nextcloud" | tee ~/.config/autostart/com.nextcloud.desktopclient.nextcloud.desktop
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.gnome.Calls.desktop && ! -f ~/.config/autostart/org.gnome.Calls.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=org.gnome.Calls
Exec=flatpak run org.gnome.Calls --daemon
X-Flatpak=org.gnome.Calls" | tee ~/.config/autostart/org.gnome.Calls.desktop
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.signal.Signal.desktop || -f ~/.local/share/flatpak/exports/share/applications/org.signal.Signal.desktop ]] && [[ ! -f ~/.config/autostart/org.signal.Signal.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=org.signal.Signal
Exec=SIGNAL_START_IN_TRAY=1 flatpak run org.signal.Signal
X-Flatpak=org.signal.Signal" | tee ~/.config/autostart/org.signal.Signal.desktop
fi

if [[ -f /var/lib/flatpak/exports/share/applications/sm.puri.Chatty.desktop && ! -f ~/.config/autostart/sm.puri.Chatty.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=sm.puri.Chatty
Exec=flatpak run sm.puri.Chatty --daemon
X-Flatpak=sm.puri.Chatty" | tee ~/.config/autostart/sm.puri.Chatty.desktop
fi

if [[ -f /etc/xdg/autostart/knmd.desktop && ! -f ~/.config/autostart/knmd.desktop ]]; then
	cp /etc/xdg/autostart/knmd.desktop .config/autostart/
	echo "Hidden=true" | tee -a .config/autostart/knmd.desktop
fi
