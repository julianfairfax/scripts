#!/bin/bash

if [[ -f /usr/lib/systemd/user/proton-bridge.service ]]; then
	systemctl enable --user --now proton-bridge.service
fi

if [[ -f ~/.config/systemd/user/proton-bridge.service ]]; then
	systemctl enable --user --now proton-bridge@$USER.service
fi

systemctl enable --user --now syncthing.service
