#!/bin/bash

sudo dnf autoremove baobab evince gnome-boxes gnome-calculator gnome-calendar gnome-characters gnome-clocks gnome-connections gnome-contacts gnome-font-viewer gnome-logs gnome-maps gnome-system-monitor gnome-text-editor gnome-tour gnome-weather libreoffice-core loupe simple-scan snapshot totem yelp gnome-abrt mediawriter

sudo flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo

sudo flatpak install --noninteractive net.nokyan.Resources org.gnome.baobab org.gnome.Calculator org.gnome.Calendar org.gnome.Characters org.gnome.clocks org.gnome.Connections org.gnome.Contacts org.gnome.Epiphany org.gnome.Evince org.gnome.font-viewer org.gnome.Logs org.gnome.Loupe org.gnome.Maps org.gnome.Music org.gnome.SimpleScan org.gnome.Snapshot org.gnome.TextEditor org.gnome.Weather org.gnome.World.PikaBackup
