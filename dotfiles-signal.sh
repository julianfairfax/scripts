echo '{
  "localeOverride": null,
  "theme-setting": "system",
  "spell-check": true,
  "window": {
    "maximized": false,
    "autoHideMenuBar": true,
    "fullscreen": false,
    "width": 874,
    "height": 582,
    "x": 0,
    "y": 0
  }
}' | tee ~/.var/app/org.signal.Signal/config/Signal/ephemeral.json
