name="proton-mail-export"
version="$(wget https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/version.txt -O - -q)"
description="Proton Mail Export"

if [[ -f ~/.$name && ! "$(cat ~/.$name)" == "$version" ]] || [[ ! -f ~/.$name ]]; then
	install -d ~/.local/bin

	if [[ $(uname -m) == "x86_64" ]]; then
		arch="amd64"
	fi

	if [[ $(uname -m) == "aarch64" ]]; then
		arch="arm64"
	fi

	wget -q https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/"$arch"/proton-mail-export-cli -O ~/.local/bin/proton-mail-export-cli

	chmod +x ~/.local/bin/proton-mail-export-cli
	
	wget -q https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/"$arch"/proton-mail-export.so -O ~/.local/lib/proton-mail-export.so

	echo "$version" | tee ~/.$name
fi
