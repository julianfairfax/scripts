[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# My Scripts

## Licensing information
All scripts in this repository have been written by me and are under the GPL-3.0 license that I have chosen to use.
