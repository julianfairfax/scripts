name="adw-gtk3"
version="$(wget https://api.github.com/repos/lassekongo83/adw-gtk3/releases/latest -O - -q | jq | grep tag_name | sed 's/.* "//' | sed 's/".*//')"
description="The theme from libadwaita ported to GTK-3"

if [[ -f ~/.$name && ! "$(cat ~/.$name)" == "$version" ]] || [[ ! -f ~/.$name ]]; then
	install -d ~/.local/share/themes

	wget -q https://github.com/lassekongo83/adw-gtk3/releases/download/$version/"$name"$version.tar.xz

	tar -xf "$name"$version.tar.xz -C ~/.local/share/themes/

	rm "$name"$version.tar.xz

	echo "$version" | tee ~/.$name
fi
