name="proton-bridge"
version="$(wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/version.txt -O - -q)"
description="Proton Mail Bridge application"

if [[ -f ~/.$name && ! "$(cat ~/.$name)" == "$version" ]] || [[ ! -f ~/.$name ]]; then
	install -d ~/.local/bin

	install -d ~/.local/lib

	install -d ~/.config/systemd/user

	if [[ $(uname -m) == "x86_64" ]]; then
		arch="amd64"
	fi

	if [[ $(uname -m) == "aarch64" ]]; then
		arch="arm64"
	fi

	wget -q https://julianfairfax.codeberg.page/proton-bridge-prebuilt/"$arch"/proton-bridge -O ~/.local/bin/"$name"

	wget -q https://julianfairfax.codeberg.page/proton-bridge-prebuilt/"$arch"/bridge -O ~/.local/bin/bridge

	chmod +x ~/.local/bin/"$name"

	chmod +x ~/.local/bin/bridge

	echo "[Unit]
Description=Proton Mail Bridge application

[Service]
Type=simple
ExecStart=/home/%i/.local/bin/$name -n

[Install]
WantedBy=default.target" | tee ~/.config/systemd/user/"$name"@.service

	echo "$version" | tee ~/.$name
fi
